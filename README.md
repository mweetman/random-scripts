# random scripts

Script | Description
------------------- | -------------
renamer             | Bash: renames files and directives recursively based on sets of rules
dupes               | Python: finds duplicate files based on hashes
gsettings_listall   | Bash: lists all Gnome 3 settings
backup_wrapper      | Bash: wrapper script around your backup commands, handles disk space management, cleaning up old backups etc
pcs_resource_status | Python: pacemaker resource status with relevant return codes
pcs_which_node      | Python: prints which node a pacemaker resource is running on
titlecase           | Python: converts a given string to title case, handy if you're not sure which words to capitalise

